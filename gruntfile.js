module.exports = function (grunt) {
    grunt.initConfig({
        copy: {
            main: {
                files: [
                    {
                        flatten: false,
                        expand: true,
                        src: ['src/**'],
                        dest: 'dist/'
                    }
                ]
            }
        },
        uglify: {
            files: {
                src: 'src/**/*.js',
                dest: 'dist',
                expand: true
            }
        },
        'json-minify': {
            build: {
                files: 'dist/**/*.json'
            }
        },
        watch: {
            js: { files: 'src/**/*.js', tasks: ['default'] },
            json: { files: 'src/**/*.json', tasks: ['default'] }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-json-minify');

    grunt.registerTask('default', ['copy', 'json-minify', 'uglify']);
    grunt.task.run('default');
};
