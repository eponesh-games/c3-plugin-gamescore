const SERVER_HOST = 'https://localhost:9000';
// const SERVER_HOST = 'https://gs.eponesh.com/sdk';

globalThis.C3.Plugins.GamePush_Channels.Instance = class GamePushChannelsInstance extends globalThis.ISDKInstanceBase {
    constructor(inst) {
        super();

        this.mappers = {
            channelAclRoleWithMyPlayer: ['myPlayer', 'owner', 'member', 'guest'],
            channelAclRole: ['owner', 'member', 'guest'],
            channelAclAction: [
                'canViewMessages',
                'canAddMessage',
                'canEditMessage',
                'canDeleteMessage',
                'canViewMembers',
                'canInvitePlayer',
                'canKickPlayer',
                'canAcceptJoinRequest',
                'canMutePlayer'
            ],
            events: [
                'event:invite',
                'event:cancelInvite',
                'event:rejectInvite',
                'event:join',
                'event:joinRequest',
                'event:leave',
                'event:cancelJoin',
                'event:rejectJoinRequest',
                'event:message',
                'event:editMessage',
                'event:deleteMessage',
                'event:mute',
                'event:unmute',
                'event:updateChannel',
                'event:deleteChannel'
            ],
            leaveReasons: ['leave', 'keck'],
            messageTargets: ['CHANNEL', 'PERSONAL', 'FEED'],
            compare: [
                (a, b) => a === b,
                (a, b) => a !== b,
                (a, b) => a < b,
                (a, b) => a <= b,
                (a, b) => a > b,
                (a, b) => a >= b
            ]
        };

        this.etos = function (err) {
            if (typeof err === 'string') {
                return err;
            }
            return 'message' in err ? err.message : String(err);
        };

        this.conditions = globalThis.C3.Plugins.GamePush_Channels.Cnds;
        this.actions = globalThis.C3.Plugins.GamePush_Channels.Acts;
        this.handleResult = (success, err) => {
            this.isLastActionSuccess = !!success;
            if (err) {
                this.lastError = this.etos(err);
                console.warn(err);
            }
        };

        this.nextChannel = {
            ownerAcl: null,
            memberAcl: null,
            guestAcl: null
        };

        this.curTag = '';
        this.curTagIndex = 0;

        this.channels = { list: [], lastRequest: '' };
        this.members = { list: [], lastRequest: '' };
        this.invites = { list: [], lastRequest: '' };
        this.joinRequests = { list: [], lastRequest: '' };
        this.messages = { list: [], lastRequest: '' };

        this.curChannel = {};
        this.curChannelIndex = 0;

        this.curMember = {};
        this.curMemberIndex = 0;

        this.curPlayer = {};
        this.curPlayerIndex = 0;

        this.curInvite = {};
        this.curInviteIndex = 0;

        this.curJoinRequest = {};
        this.curJoinRequestIndex = 0;

        this.curMessage = {};
        this.curMessageIndex = 0;

        this.curEvent = {};

        this.leaveReason = '';

        this.lastError = '';
        this.isLastActionSuccess = false;

        const properties = this._getInitProperties();
        if (properties) {
            this.isEnabled = properties[0];
            if (!this.isEnabled) {
                this.onError();
                return;
            }
        }

        this.init();
    }

    onError() {
        const stub = () => Promise.resolve({});
        this.gp = {
            player: {
                id: 0
            },
            channels: {
                on() {},
                sendInvite: stub,
                cancelInvite: stub,
                acceptInvite: stub,
                rejectInvite: stub,
                join: stub,
                leave: stub,
                cancelJoin: stub,
                acceptJoinRequest: stub,
                rejectJoinRequest: stub,
                fetchMessages: stub,
                fetchMoreMessages: stub,
                sendMessage: stub,
                sendFeedMessage: stub,
                sendPersonalMessage: stub,
                editMessage: stub,
                deleteMessage: stub,
                mute: stub,
                unmute: stub,
                fetchMembers: stub,
                fetchMoreMembers: stub,
                fetchInvites: stub,
                fetchMoreInvites: stub,
                fetchChannelInvites: stub,
                fetchMoreChannelInvites: stub,
                fetchSentInvites: stub,
                fetchMoreSentInvites: stub,
                fetchJoinRequests: stub,
                fetchMoreJoinRequests: stub,
                fetchSentJoinRequests: stub,
                fetchMoreSentJoinRequests: stub,
                kick: stub,
                fetchChannel: stub,
                fetchChannels: stub,
                fetchMoreChannels: stub,
                createChannel: stub,
                updateChannel: stub,
                deleteChannel: stub,
                openChat: stub
            }
        };
    }

    waitFor(check) {
        return new Promise((resolve) => {
            let intervalId = 0;

            function checkReady() {
                if (check(globalThis)) {
                    clearInterval(intervalId);
                    resolve();
                }
            }

            if (check(globalThis)) {
                resolve();
                return;
            }

            intervalId = setInterval(checkReady, 10);
        });
    }

    init() {
        let isReady = false;
        const iRuntime = this.runtime;
        if (iRuntime && iRuntime.GamePush) {
            this.gp = iRuntime.GamePush;
            if (!isReady) {
                isReady = true;
                this.onReady();
            }
            return;
        }

        this.waitFor(() => {
            const iRuntime = this.runtime;
            return iRuntime && iRuntime.GamePush;
        }).then(() => {
            this.gp = this.runtime.GamePush;
            if (!isReady) {
                isReady = true;
                this.onReady();
            }
        });
    }

    onReady() {
        this.gp.channels.on('event', (event) => {
            this.curEvent = event;
        });
        this.gp.channels.on('event:invite', (invite) => {
            this.curInvite = {
                ...invite,
                playerTo: invite.playerTo || { id: invite.playerToId },
                playerFrom: invite.playerFrom || { id: invite.playerFromId },
                channel: invite.channel || { id: invite.channelId }
            };
            if (this.curInvite.playerTo.id === this.gp.player.id) {
                this.curPlayer = this.curInvite.playerFrom;
            } else {
                this.curPlayer = this.curInvite.playerTo;
            }
            this.curChannel = this.curInvite.channel;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventInvite);
        });
        this.gp.channels.on('event:cancelInvite', (invite) => {
            this.curInvite = {
                ...invite,
                playerTo: invite.playerFrom || { id: invite.playerToId },
                playerFrom: invite.playerFrom || { id: invite.playerFromId },
                channel: invite.channel || { id: invite.channelId }
            };
            this.curPlayer = this.curInvite.playerTo;
            this.curChannel = this.curInvite.channel;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventCancelInvite);
        });
        this.gp.channels.on('event:rejectInvite', (invite) => {
            this.curInvite = {
                ...invite,
                playerTo: invite.playerFrom || { id: invite.playerToId },
                playerFrom: invite.playerFrom || { id: invite.playerFromId },
                channel: invite.channel || { id: invite.channelId }
            };
            this.curPlayer = this.curInvite.playerTo;
            this.curChannel = this.curInvite.channel;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventRejectInvite);
        });
        this.gp.channels.on('event:join', (member) => {
            this.curMember = member || {};
            this.curPlayer = member.state || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventJoin);
        });
        this.gp.channels.on('event:joinRequest', (joinRequest) => {
            this.curJoinRequest = joinRequest || {};
            this.curPlayer = joinRequest.player || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventJoinRequest);
        });
        this.gp.channels.on('event:leave', (leave) => {
            this.curChannel = { id: leave.channelId };
            this.curPlayer = { id: leave.playerId };
            this.leaveReason = leave.reason;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventLeave);
        });
        this.gp.channels.on('event:cancelJoin', (joinRequest) => {
            this.curChannel = { id: joinRequest.channelId };
            this.curPlayer = { id: joinRequest.playerId };
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventCancelJoin);
        });
        this.gp.channels.on('event:rejectJoinRequest', (joinRequest) => {
            this.curChannel = { id: joinRequest.channelId };
            this.curPlayer = { id: joinRequest.playerId };
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventRejectJoinRequest);
        });
        this.gp.channels.on('event:message', (message) => {
            this.curMessage = message || {};
            this.curPlayer = message.player || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventMessage);
        });
        this.gp.channels.on('event:editMessage', (message) => {
            this.curMessage = message || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventEditMessage);
        });
        this.gp.channels.on('event:deleteMessage', (message) => {
            this.curMessage = message || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventDeleteMessage);
        });
        this.gp.channels.on('event:mute', (member) => {
            this.curMember = {
                id: member.playerId,
                channelId: member.channelId,
                state: { id: member.playerId },
                mute: { isMuted: true, unmuteAt: member.unmuteAt }
            };
            this.curPlayer = this.curMember.state;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventMute);
        });
        this.gp.channels.on('event:unmute', (member) => {
            this.curMember = {
                id: member.playerId,
                channelId: member.channelId,
                state: { id: member.playerId },
                mute: { isMuted: false, unmuteAt: '' }
            };
            this.curPlayer = this.curMember.state;
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventUnmute);
        });
        this.gp.channels.on('event:updateChannel', (channel) => {
            this.curChannel = channel || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventUpdateChannel);
        });
        this.gp.channels.on('event:deleteChannel', (channel) => {
            this.curChannel = channel || {};
            this._trigger(this.conditions.OnEvent);
            this._trigger(this.conditions.OnEventDeleteChannel);
        });
        this.gp.channels.on('openChat', () => this._trigger(this.conditions.OnOpenChat));
        this.gp.channels.on('closeChat', () => {
            this.handleResult(true);
            this._trigger(this.conditions.OnCloseChat);
        });
        this.gp.channels.on('error:openChat', (err) => {
            this.handleResult(false, err);
            this._trigger(this.conditions.OnOpenChatError);
        });
    }

    Release() {
        super.Release();
    }

    SaveToJson() {
        return {
            nextChannel: this.nextChannel,
            curTag: this.curTag,
            curTagIndex: this.curTagIndex,
            channels: this.channels,
            members: this.members,
            joinRequests: this.joinRequests,
            invites: this.invites,
            messages: this.messages,
            curChannel: this.curChannel,
            curChannelIndex: this.curChannelIndex,
            curMember: this.curMember,
            curMemberIndex: this.curMemberIndex,
            curPlayer: this.curPlayer,
            curPlayerIndex: this.curPlayerIndex,
            curJoinRequest: this.curJoinRequest,
            curJoinRequestIndex: this.curJoinRequestIndex,
            curInvite: this.curInvite,
            curInviteIndex: this.curInviteIndex,
            curMessage: this.curMessage,
            curMessageIndex: this.curMessageIndex,
            curEvent: this.curEvent,
            leaveReason: this.leaveReason,
            lastError: this.lastError,
            isLastActionSuccess: this.isLastActionSuccess
        };
    }

    LoadFromJson(o) {
        this.nextChannel = o.nextChannel || {};
        this.curTag = o.curTag || '';
        this.curTagIndex = o.curTagIndex || 0;

        this.channels = o.channels || { list: [], lastRequest: '' };
        this.members = o.members || { list: [], lastRequest: '' };
        this.invites = o.invites || { list: [], lastRequest: '' };
        this.joinRequests = o.joinRequests || { list: [], lastRequest: '' };
        this.messages = o.messages || { list: [], lastRequest: '' };

        this.curChannel = o.curChannel || {};
        this.curChannelIndex = o.curChannelIndex || 0;

        this.curMember = o.curMember || {};
        this.curMemberIndex = o.curMemberIndex || 0;

        this.curPlayer = o.curPlayer || {};
        this.curPlayerIndex = o.curPlayerIndex || 0;

        this.curInvite = o.curInvite || {};
        this.curInviteIndex = o.curInviteIndex || 0;

        this.curJoinRequest = o.curJoinRequest || {};
        this.curJoinRequestIndex = o.curJoinRequestIndex || 0;

        this.curMessage = o.curMessage || {};
        this.curMessageIndex = o.curMessageIndex || 0;

        this.curEvent = o.curEvent || {};

        this.leaveReason = o.leaveReason || '';

        this.lastError = o.lastError || '';
        this.isLastActionSuccess = o.isLastActionSuccess || false;
    }

    _getDebuggerProperties() {
        return [];
    }
};
