function stoarr(str = '', type = String) {
    return String(str)
        .split(',')
        .map((o) => type(o.trim()))
        .filter((f) => f);
}

function buildChannelInput(nextChannel) {
    const channel = {};

    if (nextChannel.name) {
        channel.name = nextChannel.name;
    }

    if (nextChannel.description) {
        channel.description = nextChannel.description;
    }

    if (!isNaN(nextChannel.capacity) && nextChannel.capacity >= 0) {
        channel.capacity = nextChannel.capacity;
    }

    if (nextChannel.tags) {
        const tags = stoarr(nextChannel.tags);
        if (tags.length > 0) {
            channel.tags = tags;
        }
    }

    if ('capacity' in nextChannel && parseInt(nextChannel.capacity, 10) >= 0) {
        channel.capacity = parseInt(nextChannel.capacity, 10);
    }

    if ('private' in nextChannel) {
        channel.private = !!nextChannel.private;
    }

    if ('visible' in nextChannel) {
        channel.visible = !!nextChannel.visible;
    }

    if ('password' in nextChannel) {
        channel.password = nextChannel.password;
    }

    if (nextChannel.ownerId > 0) {
        channel.ownerId = nextChannel.ownerId;
    }

    if (nextChannel.ownerAcl) {
        channel.ownerAcl = nextChannel.ownerAcl;
    }

    if (nextChannel.memberAcl) {
        channel.memberAcl = nextChannel.memberAcl;
    }

    if (nextChannel.guestAcl) {
        channel.guestAcl = nextChannel.guestAcl;
    }

    return channel;
}

globalThis.C3.Plugins.GamePush_Channels.Acts = {
    FetchChannel(channelId) {
        return this.gp.channels
            .fetchChannel({ channelId })
            .then((channel) => {
                this.curChannel = channel;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelFetchError);
            });
    },
    FetchChannels(label, ids, tags, search, onlyOwned, onlyJoined, limit, offset) {
        return this.gp.channels
            .fetchChannels({
                ids: stoarr(ids, Number),
                tags: stoarr(tags),
                search,
                onlyOwned,
                onlyJoined,
                limit,
                offset
            })
            .then((result) => {
                this.channels.list = result.items;
                this.channels.lastRequest = 'FetchChannels';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelsFetch);
                this._trigger(this.conditions.OnChannelsAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelsFetchError);
                this._trigger(this.conditions.OnChannelsAnyFetchError);
            });
    },
    FetchMoreChannels(label, ids, tags, search, onlyOwned, onlyJoined, limit) {
        return this.gp.channels
            .fetchMoreChannels({
                ids: stoarr(ids, Number),
                tags: stoarr(tags),
                search,
                onlyOwned,
                onlyJoined,
                limit
            })
            .then((result) => {
                this.channels.list = result.items;
                this.channels.lastRequest = 'FetchChannels';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelsFetchMore);
                this._trigger(this.conditions.OnChannelsAnyFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelsFetchMoreError);
                this._trigger(this.conditions.OnChannelsAnyFetchMoreError);
            });
    },
    CreateChannel(template) {
        const query = buildChannelInput(this.nextChannel);
        query.template = template;

        this.nextChannel = {};

        return this.gp.channels
            .createChannel(query)
            .then((channel) => {
                this.curChannel = channel;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelsCreate);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelsCreateError);
            });
    },
    UpdateChannel(channelId) {
        const query = buildChannelInput(this.nextChannel);
        query.channelId = channelId;

        this.nextChannel = {};

        return this.gp.channels
            .updateChannel(query)
            .then((channel) => {
                this.curChannel = channel;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelsUpdate);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelsUpdateError);
            });
    },
    DeleteChannel(channelId) {
        return this.gp.channels
            .deleteChannel({ channelId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelsDelete);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelsDeleteError);
            });
    },

    ResetNextChannelState() {
        this.nextChannel = {};
    },
    SetNextChannelName(value) {
        this.nextChannel.name = value;
    },
    SetNextChannelDescription(value) {
        this.nextChannel.description = value;
    },
    SetNextChannelOwnerID(value) {
        this.nextChannel.ownerId = value;
    },
    SetNextChannelPassword(value) {
        this.nextChannel.password = value;
    },
    SetNextChannelCapacity(value) {
        this.nextChannel.capacity = value;
    },
    SetNextChannelVisible(value) {
        this.nextChannel.visible = value;
    },
    SetNextChannelPrivate(value) {
        this.nextChannel.private = value;
    },
    SetNextChannelTags(value) {
        this.nextChannel.tags = value;
    },
    SetNextChannelACL(
        role,
        canViewMessages,
        canAddMessage,
        canEditMessage,
        canDeleteMessage,
        canViewMembers,
        canInvitePlayer,
        canKickPlayer,
        canAcceptJoinRequest,
        canMutePlayer
    ) {
        const curRole = this.mappers.channelAclRole[role];
        this.nextChannel[`${curRole}Acl`] = {
            canViewMessages: canViewMessages,
            canAddMessage: canAddMessage,
            canEditMessage: canEditMessage,
            canDeleteMessage: canDeleteMessage,
            canViewMembers: canViewMembers,
            canInvitePlayer: canInvitePlayer,
            canKickPlayer: canKickPlayer,
            canAcceptJoinRequest: canAcceptJoinRequest,
            canMutePlayer: canMutePlayer
        };
    },
    SetNextChannelACLByInt(
        role,
        canViewMessages,
        canAddMessage,
        canEditMessage,
        canDeleteMessage,
        canViewMembers,
        canInvitePlayer,
        canKickPlayer,
        canAcceptJoinRequest,
        canMutePlayer
    ) {
        const curRole = this.mappers.channelAclRole[role];
        this.nextChannel[`${curRole}Acl`] = {
            canViewMessages: !!canViewMessages,
            canAddMessage: !!canAddMessage,
            canEditMessage: !!canEditMessage,
            canDeleteMessage: !!canDeleteMessage,
            canViewMembers: !!canViewMembers,
            canInvitePlayer: !!canInvitePlayer,
            canKickPlayer: !!canKickPlayer,
            canAcceptJoinRequest: !!canAcceptJoinRequest,
            canMutePlayer: !!canMutePlayer
        };
    },

    FetchMembers(label, channelId, search, onlyOnline, limit, offset) {
        return this.gp.channels
            .fetchMembers({
                channelId,
                search,
                onlyOnline,
                limit,
                offset
            })
            .then((result) => {
                this.members.list = result.items;
                this.members.lastRequest = 'FetchMembers';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnMembersFetch);
                this._trigger(this.conditions.OnMembersAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMembersFetchError);
                this._trigger(this.conditions.OnMembersAnyFetchError);
            });
    },
    FetchMoreMembers(label, channelId, search, onlyOnline, limit) {
        return this.gp.channels
            .fetchMoreMembers({
                channelId,
                search,
                onlyOnline,
                limit
            })
            .then((result) => {
                this.members.list = result.items;
                this.members.lastRequest = 'FetchMembers';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnMembersFetchMore);
                this._trigger(this.conditions.OnMembersAnyFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMembersFetchMoreError);
                this._trigger(this.conditions.OnMembersAnyFetchMoreError);
            });
    },
    Join(channelId, password) {
        return this.gp.channels
            .join({ channelId, password })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnJoin);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnJoinError);
            });
    },
    CancelJoin(channelId) {
        return this.gp.channels
            .cancelJoin({ channelId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnCancelJoin);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnCancelJoinError);
            });
    },
    Leave(channelId) {
        return this.gp.channels
            .leave({ channelId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnLeave);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnLeaveError);
            });
    },
    Kick(channelId, playerId) {
        return this.gp.channels
            .kick({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnKick);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnKickError);
            });
    },
    Mute(channelId, playerId, seconds) {
        return this.gp.channels
            .mute({ channelId, playerId, seconds })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnMute);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMuteError);
            });
    },
    Unmute(channelId, playerId) {
        return this.gp.channels
            .unmute({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnUnmute);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnUnmuteError);
            });
    },

    FetchJoinRequests(channelId, limit, offset) {
        return this.gp.channels
            .fetchJoinRequests({
                channelId,
                limit,
                offset
            })
            .then((result) => {
                this.joinRequests.list = result.items;
                this.joinRequests.lastRequest = 'FetchJoinRequests';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnJoinRequestsFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnJoinRequestsFetchError);
            });
    },
    FetchMoreJoinRequests(channelId, limit) {
        return this.gp.channels
            .fetchMoreJoinRequests({
                channelId,
                limit
            })
            .then((result) => {
                this.joinRequests.list = result.items;
                this.joinRequests.lastRequest = 'FetchJoinRequests';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnJoinRequestsFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnJoinRequestsFetchMoreError);
            });
    },
    FetchSentJoinRequests(limit, offset) {
        return this.gp.channels
            .fetchSentJoinRequests({
                limit,
                offset
            })
            .then((result) => {
                this.joinRequests.list = result.items;
                this.joinRequests.lastRequest = 'FetchSentJoinRequests';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnSentJoinRequestsFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnSentJoinRequestsFetchError);
            });
    },
    FetchMoreSentJoinRequests(limit) {
        return this.gp.channels
            .fetchMoreSentJoinRequests({
                limit
            })
            .then((result) => {
                this.joinRequests.list = result.items;
                this.joinRequests.lastRequest = 'FetchSentJoinRequests';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnSentJoinRequestsFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnSentJoinRequestsFetchMoreError);
            });
    },
    AcceptJoinRequest(channelId, playerId) {
        return this.gp.channels
            .acceptJoinRequest({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnAcceptJoinRequest);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnAcceptJoinRequestError);
            });
    },
    RejectJoinRequest(channelId, playerId) {
        return this.gp.channels
            .rejectJoinRequest({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnRejectJoinRequest);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnRejectJoinRequestError);
            });
    },

    FetchInvites(limit, offset) {
        return this.gp.channels
            .fetchInvites({
                limit,
                offset
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnInvitesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnInvitesFetchError);
            });
    },
    FetchMoreInvites(limit) {
        return this.gp.channels
            .fetchMoreInvites({
                limit
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnInvitesFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnInvitesFetchMoreError);
            });
    },

    FetchChannelInvites(channelId, limit, offset) {
        return this.gp.channels
            .fetchChannelInvites({
                channelId,
                limit,
                offset
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchChannelInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelInvitesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelInvitesFetchError);
            });
    },
    FetchMoreChannelInvites(channelId, limit) {
        return this.gp.channels
            .fetchMoreChannelInvites({
                channelId,
                limit
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchChannelInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnChannelInvitesFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnChannelInvitesFetchMoreError);
            });
    },
    FetchSentInvites(limit, offset) {
        return this.gp.channels
            .fetchSentInvites({
                limit,
                offset
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchSentInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnSentInvitesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnSentInvitesFetchError);
            });
    },
    FetchMoreSentInvites(limit) {
        return this.gp.channels
            .fetchMoreSentInvites({
                limit
            })
            .then((result) => {
                this.invites.list = result.items;
                this.invites.lastRequest = 'FetchSentInvites';
                this.canLoadMore = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnSentInvitesFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnSentInvitesFetchMoreError);
            });
    },
    SendInvite(channelId, playerId) {
        return this.gp.channels
            .sendInvite({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnSendInvite);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnSendInviteError);
            });
    },
    CancelInvite(channelId, playerId) {
        return this.gp.channels
            .cancelInvite({ channelId, playerId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnCancelInvite);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnCancelInviteError);
            });
    },
    AcceptInvite(channelId) {
        return this.gp.channels
            .acceptInvite({ channelId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnAcceptInvite);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnAcceptInviteError);
            });
    },
    RejectInvite(channelId) {
        return this.gp.channels
            .rejectInvite({ channelId })
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnRejectInvite);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnRejectInviteError);
            });
    },

    FetchMessages(label, channelId, tags, limit, offset) {
        return this.gp.channels
            .fetchMessages({
                tags: stoarr(tags),
                channelId,
                limit,
                offset
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesFetch);
                this._trigger(this.conditions.OnMessagesAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesFetchError);
                this._trigger(this.conditions.OnMessagesAnyFetchError);
            });
    },
    FetchMoreMessages(label, channelId, tags, limit) {
        return this.gp.channels
            .fetchMoreMessages({
                tags: stoarr(tags),
                channelId,
                limit
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesFetchMore);
                this._trigger(this.conditions.OnMessagesAnyFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesFetchMoreError);
                this._trigger(this.conditions.OnMessagesAnyFetchMoreError);
            });
    },
    FetchPersonalMessages(label, playerId, tags, limit, offset) {
        return this.gp.channels
            .fetchPersonalMessages({
                tags: stoarr(tags),
                playerId,
                limit,
                offset
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchPersonalMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnPersonalMessagesFetch);
                this._trigger(this.conditions.OnPersonalMessagesAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPersonalMessagesFetchError);
                this._trigger(this.conditions.OnPersonalMessagesAnyFetchError);
            });
    },
    FetchMorePersonalMessages(label, playerId, tags, limit) {
        return this.gp.channels
            .fetchMorePersonalMessages({
                tags: stoarr(tags),
                playerId,
                limit
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchPersonalMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnPersonalMessagesFetchMore);
                this._trigger(this.conditions.OnPersonalMessagesAnyFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPersonalMessagesFetchMoreError);
                this._trigger(this.conditions.OnPersonalMessagesAnyFetchMoreError);
            });
    },
    FetchFeedMessages(label, playerId, tags, limit, offset) {
        return this.gp.channels
            .fetchFeedMessages({
                tags: stoarr(tags),
                playerId,
                limit,
                offset
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchFeedMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnFeedMessagesFetch);
                this._trigger(this.conditions.OnFeedMessagesAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFeedMessagesFetchError);
                this._trigger(this.conditions.OnFeedMessagesAnyFetchError);
            });
    },
    FetchMoreFeedMessages(label, playerId, tags, limit) {
        return this.gp.channels
            .fetchMoreFeedMessages({
                tags: stoarr(tags),
                playerId,
                limit
            })
            .then((result) => {
                this.messages.list = result.items;
                this.messages.lastRequest = 'FetchFeedMessages';
                this.canLoadMore = result.canLoadMore;
                this.lastLabel = label;
                this.handleResult(true);
                this._trigger(this.conditions.OnFeedMessagesFetchMore);
                this._trigger(this.conditions.OnFeedMessagesAnyFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFeedMessagesFetchMoreError);
                this._trigger(this.conditions.OnFeedMessagesAnyFetchMoreError);
            });
    },
    SendMessage(channelId, text, tags) {
        return this.gp.channels
            .sendMessage({ channelId, text, tags: stoarr(tags) })
            .then((message) => {
                this.curMessage = message;
                this.curPlayer = message.player || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesSend);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesSendError);
            });
    },
    SendPersonalMessage(playerId, text, tags) {
        return this.gp.channels
            .sendPersonalMessage({ playerId, text, tags: stoarr(tags) })
            .then((message) => {
                this.curMessage = message;
                this.curPlayer = message.player || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesSend);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesSendError);
            });
    },
    SendFeedMessage(playerId, text, tags) {
        return this.gp.channels
            .sendFeedMessage({ playerId, text, tags: stoarr(tags) })
            .then((message) => {
                this.curMessage = message;
                this.curPlayer = message.player || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesSend);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesSendError);
            });
    },
    EditMessage(messageId, text) {
        return this.gp.channels
            .editMessage({ messageId, text })
            .then((message) => {
                this.curMessage = message;
                this.curPlayer = message.player || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesEdit);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesEditError);
            });
    },
    DeleteMessage(messageId) {
        return this.gp.channels
            .deleteMessage({ messageId })
            .then(() => {
                this.curMessage = { id: messageId };
                this.handleResult(true);
                this._trigger(this.conditions.OnMessagesDelete);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnMessagesDeleteError);
            });
    },

    OpenMainChat() {
        return this.gp.channels.openChat();
    },

    OpenChat(id, tags) {
        return this.gp.channels.openChat({ id, tags: stoarr(tags) });
    },

    OpenPersonalChat(playerId, tags) {
        return this.gp.channels.openPersonalChat({ playerId, tags: stoarr(tags) });
    },

    OpenFeed(playerId, tags) {
        return this.gp.channels.openFeed({ playerId, tags: stoarr(tags) });
    },

    LoadFromJSON(data) {
        try {
            const parsed = JSON.parse(data);
            if (!('isReady' in parsed)) {
                throw new Error('Data was corrupted');
            }

            this.LoadFromJson(parsed);
        } catch (error) {
            this._trigger(this.conditions.OnLoadJsonError);
        }
    }
};
