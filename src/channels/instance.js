const PLUGIN_CLASS = SDK.Plugins.GamePush_Channels;

PLUGIN_CLASS.Instance = class GamePushChannelsInstance extends globalThis.SDK.IInstanceBase {
    constructor(sdkType, inst) {
        super(sdkType, inst);
    }

    Release() {}

    OnCreate() {}

    OnPropertyChanged(id, value) {}

    LoadC2Property(name, valueString) {
        return false; // not handled
    }
};
