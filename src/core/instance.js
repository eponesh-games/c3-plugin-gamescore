const PLUGIN_CLASS = SDK.Plugins.Eponesh_GameScore;

PLUGIN_CLASS.Instance = class GameScoreInstance extends globalThis.SDK.IInstanceBase {
    constructor(sdkType, inst) {
        super(sdkType, inst);
    }

    Release() {}

    OnCreate() {}

    OnPropertyChanged(id, value) {}

    LoadC2Property(name, valueString) {
        return false; // not handled
    }
};
