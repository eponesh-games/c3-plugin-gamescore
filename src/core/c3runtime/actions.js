function stoarr(str = '', type = String) {
    return String(str)
        .split(',')
        .map((o) => type(o.trim()))
        .filter((f) => f);
}

globalThis.C3.Plugins.Eponesh_GameScore.Acts = {
    PlayerSetName(name) {
        this.gp.player.name = name;
    },

    PlayerSetAvatar(src) {
        this.gp.player.avatar = src;
    },

    PlayerSetScore(score) {
        this.gp.player.score = score;
    },

    PlayerAddScore(score) {
        this.gp.player.score += score;
    },

    PlayerSet(key, value) {
        this.gp.player.set(key, value);
    },

    PlayerSetFlag(key, value) {
        this.gp.player.set(key, !value);
    },

    PlayerAdd(key, value) {
        this.gp.player.add(key, value);
    },

    PlayerToggle(key) {
        this.gp.player.toggle(key);
    },

    PlayerReset() {
        this.gp.player.reset();
    },

    PlayerRemove() {
        this.gp.player.remove();
    },

    PlayerSync(override = false, storage) {
        return this.gp.player.sync({
            override,
            storage: this.mappers.syncStorage[storage]
        });
    },

    PlayerEnableAutoSync(interval, override = false, storage) {
        return this.gp.player.enableAutoSync({
            interval,
            override,
            storage: this.mappers.syncStorage[storage]
        });
    },

    PlayerDisableAutoSync(storage) {
        return this.gp.player.disableAutoSync({
            storage: this.mappers.syncStorage[storage]
        });
    },

    PlayerLoad() {
        return this.gp.player.load();
    },

    PlayerLogin() {
        return this.gp.player.login();
    },

    PlayerLogout() {
        return this.gp.player.logout();
    },

    PlayerFetchFields() {
        return this.gp.player.fetchFields();
    },

    PlayerWaitForReady() {
        return this.awaiters.player.ready;
    },

    LeaderboardOpen(orderBy, order, limit, withMe, includeFields, displayFields, showNearest) {
        return this.gp.leaderboard
            .open({
                id: this.gp.player.id,
                orderBy: stoarr(orderBy),
                order: order === 0 ? 'DESC' : 'ASC',
                limit,
                showNearest,
                withMe: this.mappers.withMe[withMe],
                includeFields: stoarr(includeFields),
                displayFields: stoarr(displayFields)
            })
            .catch((err) => {
                this.handleResult(false, err);
            });
    },

    LeaderboardFetch(tag, orderBy, order, limit, withMe, includeFields, showNearest) {
        return this.gp.leaderboard
            .fetch({
                id: this.gp.player.id,
                orderBy: stoarr(orderBy),
                order: order === 0 ? 'DESC' : 'ASC',
                limit,
                showNearest,
                withMe: this.mappers.withMe[withMe],
                includeFields: stoarr(includeFields)
            })
            .then((leaderboardInfo) => {
                this.lastLeaderboardTag = tag;
                this.lastLeaderboardVariant = 'default';
                this.leaderboardInfo = leaderboardInfo.leaderboard;
                this.leaderboard = leaderboardInfo.players;
                this.leaderboardResult = leaderboardInfo;
                this.handleResult(true);
                this._trigger(this.conditions.OnLeaderboardFetch);
                this._trigger(this.conditions.OnLeaderboardAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastLeaderboardTag = tag;
                this.lastLeaderboardVariant = 'default';
                this._trigger(this.conditions.OnLeaderboardFetchError);
                this._trigger(this.conditions.OnLeaderboardAnyFetchError);
            });
    },

    LeaderboardFetchPlayerRating(tag, orderBy, order, showNearest, includeFields) {
        return this.gp.leaderboard
            .fetchPlayerRating({
                showNearest,
                id: this.gp.player.id,
                orderBy: stoarr(orderBy),
                includeFields: stoarr(includeFields),
                order: order === 0 ? 'DESC' : 'ASC'
            })
            .then((result) => {
                this.lastLeaderboardTag = tag;
                this.lastLeaderboardVariant = 'default';
                this.lastLeaderboardPlayerRatingTag = tag;
                this.currentLeaderboardPlayer = Object.assign(this.gp.player.toJSON(), result.player);
                this.leaderboardPlayerPosition = result.player.position;
                this.leaderboardResult = result;
                result.topPlayers = [];
                this.handleResult(true);
                this._trigger(this.conditions.OnLeaderboardFetchPlayer);
                this._trigger(this.conditions.OnLeaderboardAnyFetchPlayer);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastLeaderboardTag = tag;
                this.lastLeaderboardVariant = 'default';
                this.lastLeaderboardPlayerRatingTag = tag;
                this._trigger(this.conditions.OnLeaderboardFetchPlayerError);
                this._trigger(this.conditions.OnLeaderboardAnyFetchPlayerError);
            });
    },

    LeaderboardOpenScoped(idOrTag, variant, order, limit, withMe, includeFields, displayFields, showNearest) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = {
            id,
            tag: idOrTag,
            variant,
            limit,
            showNearest,
            order: this.mappers.order[order],
            withMe: this.mappers.withMe[withMe],
            includeFields: stoarr(includeFields),
            displayFields: stoarr(displayFields)
        };

        return this.gp.leaderboard.openScoped(query).catch(console.warn);
    },

    LeaderboardFetchScoped(idOrTag, variant, order, limit, withMe, includeFields, showNearest) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = {
            id,
            tag: idOrTag,
            variant,
            limit,
            showNearest,
            order: this.mappers.order[order],
            withMe: this.mappers.withMe[withMe],
            includeFields: stoarr(includeFields)
        };

        return this.gp.leaderboard
            .fetchScoped(query)
            .then((leaderboardInfo) => {
                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this.leaderboardInfo = leaderboardInfo.leaderboard;
                this.leaderboard = leaderboardInfo.players;
                this.leaderboardResult = leaderboardInfo;
                this.handleResult(true);
                this._trigger(this.conditions.OnLeaderboardFetch);
                this._trigger(this.conditions.OnLeaderboardAnyFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this._trigger(this.conditions.OnLeaderboardFetchError);
                this._trigger(this.conditions.OnLeaderboardAnyFetchError);
            });
    },

    LeaderboardFetchPlayerRatingScoped(idOrTag, variant, order, showNearest, includeFields) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = {
            id,
            tag: idOrTag,
            variant,
            showNearest,
            order: this.mappers.order[order],
            includeFields: stoarr(includeFields)
        };

        return this.gp.leaderboard
            .fetchPlayerRatingScoped(query)
            .then((result) => {
                this.lastLeaderboardPlayerRatingTag = idOrTag;
                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this.currentLeaderboardPlayer = Object.assign(this.gp.player.toJSON(), result.player);
                this.leaderboardPlayerPosition = result.player.position;
                this.leaderboardResult = result;
                result.topPlayers = [];
                this.handleResult(true);
                this._trigger(this.conditions.OnLeaderboardFetchPlayer);
                this._trigger(this.conditions.OnLeaderboardAnyFetchPlayer);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastLeaderboardPlayerRatingTag = idOrTag;
                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this._trigger(this.conditions.OnLeaderboardFetchPlayerError);
                this._trigger(this.conditions.OnLeaderboardAnyFetchPlayerError);
            });
    },

    LeaderboardPublishRecord(idOrTag, variant, override) {
        const recordsTable = this.leaderboardRecords[idOrTag];
        const record = recordsTable ? recordsTable[variant] : null;

        const id = parseInt(idOrTag, 10) || 0;
        const query = {
            id,
            tag: idOrTag,
            variant,
            override,
            record
        };

        return this.gp.leaderboard
            .publishRecord(query)
            .then((result) => {
                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this.lastLeaderboardPlayerRatingTag = idOrTag;

                if (!this.leaderboardRecords[idOrTag]) {
                    this.leaderboardRecords[idOrTag] = {};
                }
                this.leaderboardRecords[idOrTag][variant] = result.record;

                this.handleResult(true);

                this._trigger(this.conditions.OnLeaderboardPublishRecord);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastLeaderboardTag = idOrTag;
                this.lastLeaderboardVariant = variant;
                this.lastLeaderboardPlayerRatingTag = idOrTag;
                this._trigger(this.conditions.OnLeaderboardPublishRecordError);
            });
    },

    LeaderboardSetRecord(idOrTag, variant, field, value) {
        if (!this.leaderboardRecords[idOrTag]) {
            this.leaderboardRecords[idOrTag] = {};
        }

        if (!this.leaderboardRecords[idOrTag][variant]) {
            this.leaderboardRecords[idOrTag][variant] = {};
        }

        this.leaderboardRecords[idOrTag][variant][field] = value;
    },

    AchievementsOpen() {
        return this.gp.achievements.open().catch(console.warn);
    },

    AchievementsFetch() {
        return this.gp.achievements
            .fetch()
            .then((result) => {
                this.achievements = result.achievements;
                this.achievementsGroups = result.achievementsGroups;
                this.playerAchievements = result.playerAchievements;
                this.handleResult(true);
                this._trigger(this.conditions.OnAchievementsFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this._trigger(this.conditions.OnAchievementsFetchError);
            });
    },

    AchievementsUnlock(idOrTag) {
        return this.gp.achievements.unlock(this.getIdOrTag(idOrTag));
    },

    AchievementsSetProgress(idOrTag, progress) {
        const query = this.getIdOrTag(idOrTag);
        query.progress = Number(progress);
        return this.gp.achievements.setProgress(query);
    },

    PaymentsFetchProducts() {
        return this.gp.payments
            .fetchProducts()
            .then((result) => {
                this.products = result.products;
                this.playerPurchases = result.playerPurchases;
                this.handleResult(true);
                this._trigger(this.conditions.OnPaymentsFetchProducts);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this._trigger(this.conditions.OnPaymentsFetchProductsError);
            });
    },

    PaymentsPurchase(idOrTag) {
        return this.gp.payments.purchase(this.getIdOrTag(idOrTag));
    },

    PaymentsConsume(idOrTag) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = id > 0 ? { id } : { tag: idOrTag };
        return this.gp.payments
            .consume(query)
            .then((result) => {
                this.isConsumeProductSuccess = result.success;
                this.consumeProductError = result.error || '';
                this.handleResult(result.success, result.error);

                const product = result.product || {};
                this.consumedProductId = product.id || 0;
                this.consumedProductTag = product.tag || '';

                if (result.success) {
                    this._trigger(this.conditions.OnPaymentsConsume);
                    this._trigger(this.conditions.OnPaymentsAnyConsume);
                    return;
                }

                this._trigger(this.conditions.OnPaymentsConsumeError);
                this._trigger(this.conditions.OnPaymentsAnyConsumeError);
            })
            .catch((err) => {
                this.isConsumeProductSuccess = false;
                this.consumeProductError = this.etos(err);
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPaymentsConsumeError);
                this._trigger(this.conditions.OnPaymentsAnyConsumeError);
            });
    },

    PaymentsSubscribe(idOrTag) {
        return this.gp.payments.subscribe(this.getIdOrTag(idOrTag));
    },

    PaymentsUnsubscribe(idOrTag) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = id > 0 ? { id } : { tag: idOrTag };
        return this.gp.payments
            .unsubscribe(query)
            .then((result) => {
                this.isUnsubscribeProductSuccess = result.success;
                this.handleResult(result.success, result.error);

                const product = result.product || {};
                const purchase = result.purchase || {};
                this.currentPurchase = purchase;
                this.currentProduct = product;
                this.currentProductPurchases = 1;

                this.purchasedProductId = product.id || 0;
                this.purchasedProductTag = product.tag || '';

                if (result.success) {
                    this._trigger(this.conditions.OnPaymentsUnsubscribe);
                    this._trigger(this.conditions.OnPaymentsAnyUnsubscribe);
                    return;
                }

                this._trigger(this.conditions.OnPaymentsUnsubscribeError);
                this._trigger(this.conditions.OnPaymentsAnyUnsubscribeError);
            })
            .catch((err) => {
                this.isUnsubscribeProductSuccess = false;
                this.handleResult(false, err);

                this._trigger(this.conditions.OnPaymentsUnsubscribeError);
                this._trigger(this.conditions.OnPaymentsAnySubscribeError);
            });
    },

    ImagesFetch(tags, playerId, limit, offset) {
        return this.gp.images
            .fetch({
                playerId,
                limit,
                offset,
                tags: stoarr(tags)
            })
            .then((result) => {
                this.images = result.items;
                this.canLoadMoreImages = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnImagesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnImagesFetchError);
            });
    },

    ImagesFetchMore(tags, playerId, limit) {
        return this.gp.images
            .fetchMore({
                playerId,
                limit,
                tags: stoarr(tags)
            })
            .then((result) => {
                this.images = result.items;
                this.canLoadMoreImages = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnImagesFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnImagesFetchMoreError);
            });
    },

    ImagesUpload(tags) {
        return this.gp.images
            .upload({ tags: stoarr(tags) })
            .then((result) => {
                this.currentImage = result || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnImagesUpload);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnImagesUploadError);
            });
    },

    ImagesUploadUrl(url, tags) {
        return this.gp.images
            .uploadUrl({ url, tags: stoarr(tags) })
            .then((result) => {
                this.currentImage = result || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnImagesUpload);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnImagesUploadError);
            });
    },

    ImagesChooseFile() {
        return this.gp.images
            .chooseFile()
            .then((result) => {
                this.lastImageTempUrl = result.tempUrl;
                this.handleResult(true);
                this._trigger(this.conditions.OnImagesChoose);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this.lastImageTempUrl = '';
                this.isLastActionSuccess = false;

                this._trigger(this.conditions.OnImagesChooseError);
            });
    },

    FilesFetch(tags, playerId, limit, offset) {
        return this.gp.files
            .fetch({
                playerId,
                limit,
                offset,
                tags: stoarr(tags)
            })
            .then((result) => {
                this.files = result.items;
                this.canLoadMoreFiles = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesFetchError);
            });
    },

    FilesFetchMore(tags, playerId, limit) {
        return this.gp.files
            .fetchMore({
                playerId,
                limit,
                tags: stoarr(tags)
            })
            .then((result) => {
                this.files = result.items;
                this.canLoadMoreFiles = result.canLoadMore;
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesFetchMore);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesFetchMoreError);
            });
    },

    FilesUpload(tags) {
        return this.gp.files
            .upload({ tags: stoarr(tags) })
            .then((result) => {
                this.currentFile = result || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesUpload);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesUploadError);
            });
    },

    FilesUploadUrl(url, filename, tags) {
        return this.gp.files
            .uploadUrl({ url, filename, tags: stoarr(tags) })
            .then((result) => {
                this.currentFile = result || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesUpload);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesUploadError);
            });
    },

    FilesUploadContent(content, filename, tags) {
        return this.gp.files
            .uploadContent({ content, filename, tags: stoarr(tags) })
            .then((result) => {
                this.currentFile = result || {};
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesUpload);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesUploadError);
            });
    },

    FilesLoadContent(url) {
        return this.gp.files
            .loadContent(url)
            .then((result) => {
                this.lastFileContent = result;
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesLoadContent);
            })
            .catch((err) => {
                this.lastFileContent = '';
                this.handleResult(false, err);
                this._trigger(this.conditions.OnFilesLoadContentError);
            });
    },

    FilesChooseFile(accept) {
        return this.gp.files
            .chooseFile(accept)
            .then((result) => {
                this.lastFileTempUrl = result.tempUrl;
                this.handleResult(true);
                this._trigger(this.conditions.OnFilesChoose);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this.lastFileTempUrl = '';
                this.isLastActionSuccess = false;

                this._trigger(this.conditions.OnFilesChooseError);
            });
    },

    VariablesFetch() {
        return this.gp.variables
            .fetch()
            .then(() => {
                this.handleResult(true);
                this._trigger(this.conditions.OnVariablesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnVariablesFetchError);
            });
    },

    PlatformVariablesFetch(clientParamsString = '') {
        const clientParams = clientParamsString.split(',').reduce((params, pair) => {
            const [key, value] = pair.split('=');
            if (key && value) {
                params[key.trim()] = value.trim();
            }
            return params;
        }, {});

        return this.gp.variables
            .fetchPlatformVariables({ clientParams })
            .then((result) => {
                this.handleResult(true);
                this.platformVariables = result;
                this._trigger(this.conditions.OnPlatformVariablesFetch);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPlatformVariablesFetchError);
            });
    },

    FullscreenOpen() {
        return this.gp.fullscreen.open();
    },

    FullscreenClose() {
        return this.gp.fullscreen.close();
    },

    FullscreenToggle() {
        return this.gp.fullscreen.toggle();
    },

    AdsShowFullscreen(showCountdownOverlay = false) {
        return this.gp.ads.showFullscreen({ showCountdownOverlay });
    },

    AdsShowRewarded(showRewardedFailedOverlay = false, tag = '') {
        this.lastRewardedTag = tag;

        return this.gp.ads.showRewardedVideo({ showRewardedFailedOverlay });
    },

    AdsShowPreloader() {
        return this.gp.ads.showPreloader();
    },

    AdsShowSticky() {
        return this.gp.ads.showSticky();
    },

    AdsCloseSticky() {
        return this.gp.ads.closeSticky();
    },

    AdsRefreshSticky() {
        return this.gp.ads.refreshSticky();
    },

    AnalyticsHit(url) {
        return this.gp.analytics.hit(url);
    },

    AnalyticsGoal(event, value) {
        return this.gp.analytics.goal(event, value);
    },

    SocialsShare(text, url, image) {
        return this.gp.socials.share({ text, url, image });
    },

    SocialsPost(text, url, image) {
        return this.gp.socials.post({ text, url, image });
    },

    SocialsInvite(text, url, image) {
        return this.gp.socials.invite({ text, url, image });
    },

    SocialsJoinCommunity() {
        return this.gp.socials.joinCommunity();
    },

    // unique values
    UniquesRegister(tag, value) {
        return this.gp.uniques.register({ tag, value });
    },

    UniquesCheck(tag, value) {
        return this.gp.uniques.check({ tag, value });
    },

    UniquesDelete(tag) {
        return this.gp.uniques.delete({ tag });
    },

    // storage
    StorageGet(key) {
        return this.gp.storage.get(key);
    },

    StorageSet(key, value) {
        return this.gp.storage.set(key, value);
    },

    StorageGetGlobal(key) {
        return this.gp.storage.getGlobal(key);
    },

    StorageSetGlobal(key, value) {
        return this.gp.storage.setGlobal(key, value);
    },

    StorageSetStorage(type) {
        return this.gp.storage.setStorage(this.mappers.storageType[type]);
    },

    // games collections
    GamesCollectionsOpen(idOrTag) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = id > 0 ? { id } : { tag: idOrTag || 'ANY' };
        return this.gp.gamesCollections.open(query);
    },

    GamesCollectionsFetch(idOrTag) {
        const id = parseInt(idOrTag, 10) || 0;
        const query = id > 0 ? { id } : { tag: idOrTag };
        return this.gp.gamesCollections
            .fetch(query)
            .then((result) => {
                this.lastGamesCollectionIdOrTag = idOrTag;
                this.gamesCollection = result;
                this._trigger(this.conditions.OnGamesCollectionsFetch);
                this._trigger(this.conditions.OnGamesCollectionsFetchAny);
            })
            .catch((err) => {
                this.handleResult(false, err);

                this.lastGamesCollectionIdOrTag = idOrTag;
                this.gamesCollectionFetchError = (err && err.message) || '';
                this._trigger(this.conditions.OnGamesCollectionsFetchError);
                this._trigger(this.conditions.OnGamesCollectionsFetchAnyError);
            });
    },

    // documents
    DocumentsOpen(docType) {
        const type = this.mappers.documentTypes[docType];
        return this.gp.documents.open({ type });
    },

    DocumentsFetch(docType, docFormat) {
        const type = this.mappers.documentTypes[docType];
        const format = this.mappers.documentFormat[docFormat];
        return this.gp.documents
            .fetch({ type, format })
            .then((result) => {
                this.lastDocumentType = type;
                this.document = result;
                this._trigger(this.conditions.OnDocumentsFetch);
                this._trigger(this.conditions.OnDocumentsFetchAny);
            })
            .catch((err) => {
                this.handleResult(false, err);
                this.documentFetchError = this.etos(err);

                this.lastDocumentType = type;
                this._trigger(this.conditions.OnDocumentsFetchError);
                this._trigger(this.conditions.OnDocumentsFetchAnyError);
            });
    },

    // players
    PlayersFetch(tag, ids) {
        return this.gp.players
            .fetch({ ids: stoarr(ids, Number) })
            .then((result) => {
                this.lastPlayersTag = tag;
                this.playersList = result.players;
                this.handleResult(true);
                this._trigger(this.conditions.OnPlayersFetch);
                this._trigger(this.conditions.OnPlayersAnyFetch);
            })
            .catch((err) => {
                this.lastPlayersTag = tag;
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPlayersFetchError);
                this._trigger(this.conditions.OnPlayersAnyFetchError);
            });
    },

    PlayersFetchOne(tag, id) {
        return this.gp.players
            .fetch({ ids: [Number(id)] })
            .then((result) => {
                this.lastPlayersTag = tag;
                if (result.players[0]) {
                    this.playersList = result.players;
                    this.currentPlayersIndex = 0;
                    this.currentPlayersPlayer = result.players[0];
                    this.handleResult(true);
                    this._trigger(this.conditions.OnPlayersFetch);
                    this._trigger(this.conditions.OnPlayersAnyFetch);
                } else {
                    this.handleResult(false, new Error('player_not_found'));
                    this._trigger(this.conditions.OnPlayersFetchError);
                    this._trigger(this.conditions.OnPlayersAnyFetchError);
                }
            })
            .catch((err) => {
                this.lastPlayersTag = tag;
                this.handleResult(false, err);
                this._trigger(this.conditions.OnPlayersFetchError);
                this._trigger(this.conditions.OnPlayersAnyFetchError);
            });
    },

    // windows

    WindowsShowConfirm(title, description, textConfirm, textCancel, invertButtonColors) {
        return this.gp.windows
            .showConfirm({ title, description, textConfirm, textCancel, invertButtonColors })
            .catch(console.warn);
    },

    // app
    AppAddShortcut() {
        return this.gp.app.addShortcut();
    },
    AppRequestReview() {
        return this.gp.app.requestReview();
    },

    ChangeLanguage(language) {
        return this.gp.changeLanguage(this.mappers.language[language]);
    },

    ChangeLanguageByCode(language = '') {
        return this.gp.changeLanguage(language.toLowerCase());
    },

    ChangeAvatarGenerator(generator) {
        return this.gp.changeAvatarGenerator(this.mappers.avatarGenerator[generator]);
    },

    SetBackground(url, blur, fade) {
        return this.gp.setBackground({ url, blur, fade });
    },

    LoadOverlay() {
        return this.gp.loadOverlay();
    },

    Pause() {
        return this.gp.pause();
    },

    Resume() {
        return this.gp.resume();
    },

    GameStart() {
        return this.gp.gameStart();
    },

    GameplayStart() {
        return this.gp.gameplayStart();
    },

    GameplayStop() {
        return this.gp.gameplayStop();
    },

    SocialsAddShareUrlParam(key, value) {
        this.shareParams[key] = value;
    },

    SocialsResetShareParams() {
        this.shareParams = {};
    },

    RewardsAccept(idOrTag) {
        return this.gp.rewards.accept(this.getIdOrTag(idOrTag));
    },
    RewardsGive(idOrTag, lazy) {
        const query = this.getIdOrTag(idOrTag);
        query.lazy = !!lazy;
        return this.gp.rewards.give(query);
    },

    TriggersClaim(idOrTag) {
        return this.gp.triggers.claim({ id: idOrTag, tag: idOrTag });
    },

    SchedulersRegister(idOrTag) {
        return this.gp.schedulers.register(this.getIdOrTag(idOrTag));
    },
    SchedulersClaimDay(idOrTag, day) {
        return this.gp.schedulers.claimDay(idOrTag, day);
    },
    SchedulersClaimDayAdditional(idOrTag, day, triggerIdOrTag) {
        return this.gp.schedulers.claimDayAdditional(idOrTag, day, triggerIdOrTag);
    },
    SchedulersClaimAllDay(idOrTag, day) {
        return this.gp.schedulers.claimAllDay(idOrTag, day);
    },
    SchedulersClaimAllDays(idOrTag) {
        return this.gp.schedulers.claimAllDays(idOrTag);
    },

    EventsJoin(idOrTag) {
        return this.gp.events.join(this.getIdOrTag(idOrTag));
    },

    LoadFromJSON(data) {
        try {
            const parsed = JSON.parse(data);
            if (!('isReady' in parsed)) {
                throw new Error('Data was corrupted');
            }

            this.LoadFromJson(parsed);
        } catch (error) {
            this._trigger(this.conditions.OnLoadJsonError);
        }
    }
};
